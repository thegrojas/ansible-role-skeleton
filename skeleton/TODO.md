## To do

### README
- [ ] Update Logos
- [ ] Update Badges
- [ ] Update Role Name and Summary
- [ ] Document Requirements
- [ ] Document Role Variables
- [ ] Document Dependencies
- [ ] Document Example Playbook
- [ ] Document License
- [ ] Document Author Information
### Document variables on `defaults/`
### CHANGELOG
- [ ] Set up a CHANGELOG.md
### CONTRIBUTING
- [ ] Set up a CONTRIBUTING.md
### LICENSE
- [ ] Set up a LICENSE
### Ansible Galaxy
- [ ] Update `meta/main.yml`
- [ ] Add repo to Ansible Galaxy

## In Progress


## Done

